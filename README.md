# Sabis.EventBus

### Kurulum

Sabis.EventBus sabis.nuget'i tarafından kurulabilir.

### ServiceProvider'a Eklenmesi
Startup.cs dosyasında;
```sh
services.AddSabisEventBus(builder =>
            {
                builder.AddInMemoryEventBus(subscriber =>
                {
                    //subscriber.Subscribe<MyEvent, MyFirstEventHandler>(); //Manual subscriber
                    subscriber.SubscribeAllHandledEvents<MyFirstEventHandler>(); // Generic subscriber
                });
            });
```
şeklinde service provider'a eklenir. 
> Generic subscriber, **Event** tipi belirtmeden o handler'a tanımlanan tüm event'leri otomatik olarak subscribe eder ve kullanım kolaylığı sağlar.


### EventBus Injection
Constructor Injection ile **IEventBus** alttaki örnekteki gibi inject edilebilir.
```sh
using Sabis.EventBus;
public class HomeController : Controller
    {
        private readonly IEventBus _eventBus;
        public HomeController(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }
    }
```
### Kullanım

Örnek Event:
```sh
using Sabis.EventBus;
public class MyEvent 
    {
        public string MyMessage { get; set; }
		public MyEvent(string message)
        {
            MyMessage = message;
        }
    }
```

Örnek EventHandler:
```sh
using Sabis.EventBus;
public class MyFirstEventHandler : IEventHandler<MyEvent>
    {
        public Task HandleEventAsync(MyEvent @event)
        {
            string handlerName = "MyFirstEventHandler";
            Console.WriteLine($"Received message from {handlerName}: '{@event.Message}'");
            return Task.CompletedTask;
        }
    }
```

> Event'ler ve Handler'lar için ayrı klasörler oluşturup kullanmanız ileride kod karışıklığını engeller.

#### Event Publish Etme
Inject edilen EventBus ile alttaki şekilde event publish edebilirsiniz. 
```sh
await _eventBus.PublishEventAsync<MyBasicEvent>(new MyEvent("this is my message!"));
```

#### Not
Bir EventHandler biden çok Event'i handle edebilir.
```sh
public class MySuperEventHandler : IEventHandler<MyBasicEvent>, IEventHandler<MySecondEvent>, IEventHandler<MyThirdEvent>
    {
        public Task HandleEventAsync(MyBasicEvent @event)
        {
            ...
        }

        public Task HandleEventAsync(MySecondEvent @event)
        {
            ...
        }

        public Task HandleEventAsync(MyThirdEvent @event)
        {
            ...
        }
    }
```
