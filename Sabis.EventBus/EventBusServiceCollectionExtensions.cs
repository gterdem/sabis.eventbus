﻿using Sabis.EventBus.DependencyInjection;
using System;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// EventBus Service Collection Extension for DI
    /// </summary>
    public static class EventBusServiceCollectionExtensions
    {
        /// <summary>
        /// Adds Sabis EventBus to the service collection.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="setupAction"></param>
        /// <returns></returns>
        public static IServiceCollection AddSabisEventBus(this IServiceCollection services,
            Action<IEventBusBuilder> setupAction)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            IEventBusBuilder builder = new EventBusBuilder(services)
                .UseJsonSerializer();

            setupAction?.Invoke(builder);

            return services;
        }
    }
}
