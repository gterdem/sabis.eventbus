﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Sabis.EventBus.Serialization
{
    /// <summary>
    /// JsonEventSerializer implementation.
    /// </summary>
    public class JsonEventSerializer : IEventSerializer
    {
        private static readonly JsonSerializerSettings _jsonSerializerSettings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.Objects,
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            NullValueHandling = NullValueHandling.Ignore,
            Formatting = Formatting.None,
            Converters = new[] { new StringEnumConverter() },
        };
        /// <summary>
        /// Event Serialization Method.
        /// </summary>
        /// <typeparam name="TEvent"></typeparam>
        /// <param name="event"></param>
        /// <returns></returns>
        public string Serialize<TEvent>(TEvent @event)
        {
            return JsonConvert.SerializeObject(@event, _jsonSerializerSettings);
        }
    }
}
