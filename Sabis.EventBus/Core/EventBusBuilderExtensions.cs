﻿using Sabis.EventBus;
using Sabis.EventBus.InMemory;
using Sabis.EventBus.Serialization;
using Sabis.EventBus.Subscription;
using System;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Builder extension for the EventBus
    /// </summary>
    public static class EventBusBuilderExtensions
    {
        /// <summary>
        /// Adds EventBus as an In-Memory service.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="subscribeAction"></param>
        /// <returns></returns>
        public static IEventBusBuilder AddInMemoryEventBus(this IEventBusBuilder builder, Action<IEventBusSubscriber> subscribeAction)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            var subscriber = new InMemoryEventBusSubscriber(builder.Services);
            subscribeAction?.Invoke(subscriber);

            return builder.AddEventPublisher<InMemoryEventBus>();
        }
        /// <summary>
        /// Using JsonSerializer
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static IEventBusBuilder UseJsonSerializer(this IEventBusBuilder builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            return builder.UseSerializer<JsonEventSerializer>();
        }
    }
}
