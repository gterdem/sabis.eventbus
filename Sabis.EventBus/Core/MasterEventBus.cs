﻿using System.Threading.Tasks;

namespace Sabis.EventBus
{
    internal class MasterEventBus : IEventBus
    {
        private readonly IEventBusProvider _eventPublishers;
        /// <summary>
        /// Event Publisher implementation.
        /// </summary>
        /// <param name="eventPublishers"></param>
        public MasterEventBus(
            IEventBusProvider eventPublishers)
        {
            _eventPublishers = eventPublishers;
        }
        /// <summary>
        /// Publishing event implementation.
        /// </summary>
        /// <typeparam name="TEvent"></typeparam>
        /// <param name="event"></param>
        /// <returns></returns>
        public async Task PublishEventAsync<TEvent>(TEvent @event) where TEvent : class
        {
            foreach (IEventBus publisher in _eventPublishers.GetEventPublishers())
            {
                await publisher.PublishEventAsync(@event);
            }
        }
    }
}
