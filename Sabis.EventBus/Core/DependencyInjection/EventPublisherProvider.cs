﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sabis.EventBus.DependencyInjection
{
    class EventPublisherProvider : IEventBusProvider
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly EventPublisherRegister _eventPublishers;

        public EventPublisherProvider(
            IServiceProvider serviceProvider,
            EventPublisherRegister eventPublishers)
        {
            _serviceProvider = serviceProvider;
            _eventPublishers = eventPublishers;
        }


        public IEnumerable<IEventBus> GetEventPublishers()
        {
            return _eventPublishers.Types.Select(t => (IEventBus)_serviceProvider.GetService(t));
        }
    }
}
