﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sabis.EventBus.DependencyInjection
{
    /// <summary>
    /// Event Publisher Registration
    /// </summary>
    public class EventPublisherRegister
    {
        private readonly HashSet<Type> _types = new HashSet<Type>();
        /// <summary>
        /// Returns the types of event publishers in readonly list.
        /// </summary>
        public IEnumerable<Type> Types => _types.ToList().AsReadOnly();

        internal void Add<TEventPublisher>()
            where TEventPublisher : class, IEventBus
        {
            _types.Add(typeof(TEventPublisher));
        }
    }
}
