﻿using Microsoft.Extensions.DependencyInjection;

namespace Sabis.EventBus.DependencyInjection
{
    /// <summary>
    /// EventBus builder implementation.
    /// </summary>
    public class EventBusBuilder : IEventBusBuilder
    {
        private readonly EventPublisherRegister _register = new EventPublisherRegister();
        /// <summary>
        /// Injection of EventPublisher as Singleton service.
        /// </summary>
        /// <param name="services"></param>
        public EventBusBuilder(IServiceCollection services)
        {
            Services = services
                .AddSingleton(_register)
                .AddSingleton<IEventBusProvider, EventPublisherProvider>()
                .AddSingleton<IEventBus, MasterEventBus>();
        }
        /// <summary>
        /// DI Service Collection.
        /// </summary>
        public IServiceCollection Services { get; }
        /// <summary>
        /// Injection of Event Serializer as Singleton service.
        /// </summary>
        /// <typeparam name="TEventSerializer"></typeparam>
        /// <returns></returns>
        public IEventBusBuilder UseSerializer<TEventSerializer>()
            where TEventSerializer : class, IEventSerializer
        {
            Services.AddSingleton<IEventSerializer, TEventSerializer>();
            return this;
        }
        /// <summary>
        /// Injection of other EventPublisher (like InMemory Publisher) as a Singleton Service
        /// </summary>
        /// <typeparam name="TEventPublisher"></typeparam>
        /// <returns></returns>
        public IEventBusBuilder AddEventPublisher<TEventPublisher>()
            where TEventPublisher : class, IEventBus
        {
            _register.Add<TEventPublisher>();
            Services.AddSingleton<TEventPublisher>();
            return this;
        }
    }
}
