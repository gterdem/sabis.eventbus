﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

namespace Sabis.EventBus.InMemory
{
    /// <summary>
    /// In Memory Event Bus implementation.
    /// </summary>
    public class InMemoryEventBus : IEventBus
    {
        private readonly IServiceProvider _serviceProvider;
        /// <summary>
        /// InMemoryServiceBus .
        /// </summary>
        /// <param name="serviceProvider"></param>
        public InMemoryEventBus(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        /// <summary>
        /// Publishing event implementation by InMemoryServiceBus in created service scope.
        /// </summary>
        /// <typeparam name="TEvent"></typeparam>
        /// <param name="event"></param>
        /// <returns></returns>
        public async Task PublishEventAsync<TEvent>(TEvent @event) where TEvent : class
        {
            using (IServiceScope scope = _serviceProvider.CreateScope())
            {
                Type eventType = @event.GetType();
                Type openHandlerType = typeof(IEventHandler<>);
                Type handlerType = openHandlerType.MakeGenericType(eventType);
                IEnumerable<object> handlers = scope.ServiceProvider.GetServices(handlerType);
                foreach (object handler in handlers)
                {
                    object result = handlerType
                        .GetTypeInfo()
                        .GetDeclaredMethod(nameof(IEventHandler<TEvent>.HandleEventAsync))
                        .Invoke(handler, new object[] { @event });
                    await (Task)result;
                }
            }
        }
    }
}
