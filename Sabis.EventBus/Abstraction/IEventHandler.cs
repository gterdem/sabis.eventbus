﻿using System.Threading.Tasks;

namespace Sabis.EventBus
{
    /// <summary>
    /// Event Handler abstraction for the Event.
    /// </summary>
    /// <typeparam name="TEvent"></typeparam>
    public interface IEventHandler<TEvent> where TEvent : class
    {
        /// <summary>
        /// Event Handling abstraction.
        /// </summary>
        /// <param name="event"></param>
        /// <returns></returns>
        Task HandleEventAsync(TEvent @event);
    }
}
