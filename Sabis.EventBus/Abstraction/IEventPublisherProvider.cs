﻿using System.Collections.Generic;

namespace Sabis.EventBus
{
    /// <summary>
    /// Event Publisher Provider.
    /// </summary>
    public interface IEventBusProvider
    {
        /// <summary>
        /// Abstraction for getting all EventPublishers.
        /// </summary>
        /// <returns></returns>
        IEnumerable<IEventBus> GetEventPublishers();
    }
}
