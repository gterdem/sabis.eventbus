﻿namespace Sabis.EventBus
{
    /// <summary>
    /// Event Serializer Abstraction.
    /// </summary>
    public interface IEventSerializer
    {
        /// <summary>
        /// Event Serialize.
        /// </summary>
        /// <typeparam name="TEvent"></typeparam>
        /// <param name="event"></param>
        /// <returns></returns>
        string Serialize<TEvent>(TEvent @event);
    }
}
