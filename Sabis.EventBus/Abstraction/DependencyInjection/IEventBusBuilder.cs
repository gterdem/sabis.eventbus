﻿using Sabis.EventBus;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Event Bus Builder Abstraction.
    /// </summary>
    public interface IEventBusBuilder
    {
        /// <summary>
        /// DI readonly Service Collection.
        /// </summary>
        IServiceCollection Services { get; }

        /// <summary>
        /// Serializer abstraction.
        /// </summary>
        /// <typeparam name="TEventSerializer"></typeparam>
        /// <returns></returns>
        IEventBusBuilder UseSerializer<TEventSerializer>()
            where TEventSerializer : class, IEventSerializer;

        /// <summary>
        /// Adding event publisher abstraction.
        /// </summary>
        /// <typeparam name="TEventPublisher"></typeparam>
        /// <returns></returns>
        IEventBusBuilder AddEventPublisher<TEventPublisher>()
            where TEventPublisher : class, IEventBus;
    }
}
