﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sabis.EventBus
{
    /// <summary>
    /// EventBus subscriber abstraction.
    /// </summary>
    public interface IEventBusSubscriber
    {
        /// <summary>
        /// Subscribe Events to Handler.
        /// </summary>
        /// <typeparam name="TEvent"></typeparam>
        /// <typeparam name="TEventHandler"></typeparam>
        /// <returns></returns>
        IEventBusSubscriber Subscribe<TEvent, TEventHandler>()
            where TEvent : class
            where TEventHandler : class, IEventHandler<TEvent>;

        /// <summary>
        /// Automatically subscribes all related Events to the Handler.
        /// </summary>
        /// <typeparam name="TEventHandler"></typeparam>
        /// <returns></returns>
        IEventBusSubscriber SubscribeAllHandledEvents<TEventHandler>()
            where TEventHandler : class;
    }
}
