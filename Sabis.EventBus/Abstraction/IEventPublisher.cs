﻿using System.Threading.Tasks;

namespace Sabis.EventBus
{
    /// <summary>
    /// EventBus abstraction for injection.
    /// </summary>
    public interface IEventBus
    {
        /// <summary>
        /// Publish Event to the EventBus.
        /// </summary>
        /// <typeparam name="TEvent"></typeparam>
        /// <param name="event"></param>
        /// <returns></returns>
        Task PublishEventAsync<TEvent>(TEvent @event) where TEvent : class;
    }
}
