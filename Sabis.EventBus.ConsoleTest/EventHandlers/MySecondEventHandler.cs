﻿using Sabis.EventBus.ConsoleTest.Events;
using System;
using System.Threading.Tasks;

namespace Sabis.EventBus.ConsoleTest.EventHandlers
{
    class MySecondEventHandler : IEventHandler<MySecondEvent>
    {
        public Task HandleEventAsync(MySecondEvent @event)
        {
            string handlerName = "MySecondEventHandler";
            Console.WriteLine($"Received message from {handlerName}: '{@event.MyAge}'");
            return Task.CompletedTask;
        }
    }
}
