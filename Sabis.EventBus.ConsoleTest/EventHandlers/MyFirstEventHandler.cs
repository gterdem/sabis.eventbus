﻿using Sabis.EventBus.ConsoleTest.Events;
using System;
using System.Threading.Tasks;

namespace Sabis.EventBus.ConsoleTest.EventHandlers
{
    class MyFirstEventHandler : IEventHandler<MyEvent>
    {
        public Task HandleEventAsync(MyEvent @event)
        {
            string handlerName = "MyFirstEventHandler";
            Console.WriteLine($"Received message from {handlerName}: '{@event.Message}'");
            return Task.CompletedTask;
        }
    }
}
