﻿using Sabis.EventBus.ConsoleTest.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.EventBus.ConsoleTest.EventHandlers
{
    class MasterEventHandler : IEventHandler<MyEvent>, IEventHandler<MySecondEvent>
    {
        public Task HandleEventAsync(MySecondEvent @event)
        {
            throw new NotImplementedException();
        }

        public Task HandleEventAsync(MyEvent @event)
        {
            throw new NotImplementedException();
        }
    }
}
