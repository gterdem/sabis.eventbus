﻿using Microsoft.Extensions.DependencyInjection;
using Sabis.EventBus.ConsoleTest.EventHandlers;
using Sabis.EventBus.ConsoleTest.Events;
using System;

namespace Sabis.EventBus.ConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            IServiceCollection services = new ServiceCollection();

            services.AddSabisEventBus(builder =>
            {
                builder.AddInMemoryEventBus(subscriber =>
                {
                    subscriber.Subscribe<MyEvent, MyFirstEventHandler>();
                    subscriber.Subscribe<MyEvent, MyFirstEventHandler>();
                    subscriber.Subscribe<MyEvent, MyFirstEventHandler>();
                    subscriber.Subscribe<MySecondEvent, MySecondEventHandler>();
                    subscriber.SubscribeAllHandledEvents<MyFirstEventHandler>();
                    subscriber.SubscribeAllHandledEvents<MySecondEventHandler>();
                });
            });

            string msg = "Hello, this is my event msg";
            IServiceProvider serviceProvider = services.BuildServiceProvider();
            using (IServiceScope scope = serviceProvider.CreateScope())
            {
                IEventBus eventPublisher = scope.ServiceProvider.GetRequiredService<IEventBus>();
                eventPublisher.PublishEventAsync(new MyEvent { Message = msg }).Wait();
                eventPublisher.PublishEventAsync(new MySecondEvent { MyAge = 15 }).Wait();
            }
            Console.ReadLine();
        }
    }
}
