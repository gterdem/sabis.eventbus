﻿namespace Sabis.EventBus.ConsoleTest.Events
{
    public class MyEvent
    {
        public string Message { get; set; }
    }
    public class MySecondEvent
    {
        public int MyAge { get; set; }
    }
}
